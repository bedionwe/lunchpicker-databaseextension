# Lunchpicker DatabaseExtension

Extending the Lunchpicker Project (https://gitlab.com/bedionwe/halbjahresprojekt) with the ability to store recipes to your local Database.

### Projektziel

Es soll der Umgang mit Datenbank innerhalb von C# erlernt werden. Die Datenbanken können über DataSets leicht in das Projekt eingebunden und dementsprechend bearbeitet werden. 

In diesem Projekt soll der Lunchpicker aus dem vorherigen Jahr erweitert werden. Es soll möglich sein einzelne Rezepte in eine Datenbank abzulegen und einzusehen. Die gespeicherten Rezepte in der Datenbank können dann über ein Datagrid angezeigt werden. wird ein Rezept ausgewählt wird die Einkaufsliste mit den Zutaten sowie Zubereitung angezeigt. 

Weiters soll es möglich sein die Datenbankeinträge zu bearbeiten und auch zu löschen. Die Rezepte können wie in der ursprünglichen Lunchpicker-Version auch lokal angelegt und abgespeichert werden, von dort aus, können sie dann in die Datenbank exportiert werden. 

![alt text](/Images/lunchpickerUI.jpg)

### Umsetzung

#### Datenbank-Tabellen 

Um das Projekt umzusetzen wird zuerst die Datenbank "lunchpicker" angelegt, die Struktur der Tabellen dieser Datenbank wird im Folgenden erläutert.  

**Struktur der recipe-Tabelle**

| recipe - columns | Erklärung                    |
| ---------------- | ---------------------------- |
| ID               | (AutoIncrement)              |
| identifier       | interner Index (Lunchpicker) |
| date             | Erstellungsdatum des Rezepts |
| name             | Name des Rezepts             |
| manual           | Zubereitung                  |

**Struktur der ingredient-Tabelle** 

| ingredient - columns | Erklärung                    |
| -------------------- | ---------------------------- |
| ID                   | (AutoIncrement)              |
| identifier           | interner Index (Lunchpicker) |
| type                 | Name der Zutat               |
| amount               | Menge                        |
| unit                 | Einheit                      |

Die identifier bestehen aus 36 Bit GUIDs  und dienen der Ordnung und Struktur im Projekt. Weiters sind sie hier für die Zuordnung der Rezepte zu den jeweiligen Zutaten wichtig, denn die Zutaten werden in einer eigenen Tabelle gespeichert, das hat den Vorteil, dass die jeweiligen Zutaten sehr leicht in einem eigenen Datagrid angezeigt werden können. 

Die Verwendung von Views wäre möglich gewesen, unterschiedliche Views sind auch angelegt worden, allerdings ist deren Anwendung nur ein Mehraufwand, und nicht wirklich Praktisch in diesem Kontext. 

#### Datasets und verwendete Queries 

![alt text](/Images/datasets.jpg)

Es wurden Datasets für die Intredients und Recipes hinzugefügt. die jeweiligen TableAdapter haben unterschiedliche Queries, die im Programmablauf genutzt werden. 

**ingredientsTableAdapter**

| Queries           | Nutzen                                                 |
| ----------------- | ------------------------------------------------------ |
| Fill              | Gibt Tabelleninhalt zurück                             |
| DeleteIngredients | Löscht gewisse Zutaten-Einträge                        |
| GetIngForRec      | Fragt die zugehörigen Zutaten zum jeweiligen Rezept ab |
| InsertIngredient  | Fügt Zutaten in die Liste ein                          |
| UpdateIngredient  | Bearbeitet Zutateneinträge                             |

Im Normalfall würde die ID dafür zuständig sein, die Einträge voneinander zu unterscheiden und diese zu "adressieren". Da das ursprüngliche Lunchpicker-Programm aber schon mit einer GUID (hier der identifier) arbeitet, wird dieser hier zur Adressierung genutzt. Weiters muss festgestellt werden können, welche Zutaten zu einem Rezept gehören, auch dazu wird die GUID verwendet. 

**recipeTableAdapter**

| Queries          | Nutzen                          |
| ---------------- | ------------------------------- |
| Fill             | Gibt Tabelleninhalt zurück      |
| DeleteRecipe     | Löscht gewisse Rezept-Einträge  |
| InsertIngredient | Fügt Rezepte in die Tabelle ein |
| UpdateIngredient | Bearbeitet Rezepteieinträge     |

#### Bindings

Mittels Binding lässt sich der Inhalt der beiden Datagrids (die Tabellen in denen die Daten angezeigt werden) sehr einfach bestimmen.

Im XAML-Code muss dafür nur der die ItemsSource angepasst werden. 

```c#
<DataGrid Margin="0,5,0,5" x:Name="dgrRecipeDate" Width="790" Height="300" SelectionUnit="FullRow" AutoGenerateColumns="False" SelectionMode="Single" ItemsSource="{Binding}" SelectionChanged="dgrRecipeDate_SelectionChanged" Background="White" BorderBrush="#6E4453" IsReadOnly="True">
	....
</DataGrid>
```

Durch das Hinzufügen des richtigen DataContextes wird der Inhalt der Tabelle automatisch eingetragen. 

```C#
recipeDataTable = dataSet.recipe;
recipeTableAdapter.Fill(recipeDataTable);            
dgrRecipeDate.DataContext = recipeDataTable;
```

#### Zutaten - Tabelle 

Um den Inhalt der Tabelle zu ändern, sobald sich das ausgewählte Element im Rezepte-Datagrid ändert, muss das Event abgefragt werden. 

Ein Weiterer Kernpunkt ist, dass die Zutaten des jeweiligen Rezepts angezeigt werden sollen, um das zu erreichen, wird der identifier, aus der Rezept-Tabelle in der Datenbank abgefragt. Gibt man bei DataGrids die ItemsSource an, so kann entschieden werden, welche Spalten der Datenbanktabelle auch im Datagrid zu sehen sein sollen, man kann allerdings auch auf die anderen Spalten zugreifen.

````C#
private void dgrRecipeDate_SelectionChanged(object sender, SelectionChangedEventArgs e)
{
	if (dgrRecipeDate.SelectedIndex >= 0)
    {
    	DataRowView row = (DataRowView)dgrRecipeDate.SelectedItem;
        editedIdentifier = row["identifier"].ToString(); 
                
        tbDBIngredient.Text = row["manual"].ToString();
        var tempIngredient = ingredientTableAdapter.GetIngForRec(editedIdentifier);
        dgrIngredient.DataContext = tempIngredient;
     }

}
````

Dadurch lässt sich der Identifier abfragen, mittels welchem die zugehörigen Zutaten herausgesucht  und angezeigt werden können. Auch die Zubereitung (manual) lässt sich direkt aus der Tabelle entnehmen.

Im Anschluss wird der DataContext des Datagrids für die Zutaten festgelegt. 

 #### Rezepte Hochladen

In dieser Anwendung können bereits lokal existente Rezepte in die Datenbank aufgenommen werden. Dazu wird der recipeTableAdapter sowie der ingredientTableAdapter benötigt. 

Zuerst muss überprüft werden, ob es das Rezept in der Datenbank schon gibt, die Überprüfung findet mittels Identifier statt. Ist diese Voraussetzung gegeben, so kann das Rezept in die Datenbank eingetragen werden. 

````C#
 recipeTableAdapter.insertRecipe(name, manual, date, identifier);
 
 foreach (Ingredient item in rToSave.IngredientList)
 {
 	ingredientTableAdapter.InsertIngredient(identifier, item.Type,(decimal)item.Amount, item.Unit.ToString());
                
}
````

`rToSave` entspricht dabei einem lokalen Recipe-Objekt.

Nachdem nun an der Datenbank etwas geändert wird, ist es wichtig die TableAdapter zu aktualisieren, durch das DataBinding, werden auch die DataGrids aktualisiert. 

````
recipeTableAdapter.Update(recipeDataTable);
recipeTableAdapter.Fill(recipeDataTable);

ingredientTableAdapter.Update(ingredientDataTable);
ingredientTableAdapter.Fill(ingredientDataTable);
````



#### Rezepte bearbeitend

Um Rezepte zu bearbeiten muss der Nutzer ein Rezept aus dem Datagrid auswählen und dann den Bearbeiten-Butten betätigen. 

Das Fenster, welches jetzt angezeigt wird, entspricht dem Bearbeitungsfenster des Originalen Lunchpickers. 

Damit das Bearbeiten Benutzerfreundlich bleibt, müssen die Werte die sich in der Datenbank befinden, in das Bearbeitungsfenster geladen werden. 

![alt text](/Images/bearbeitungsfenster.jpg)

Die Boolliste wird hier nicht beachtet, da es den Zeitrahmen des Projekts sprengen würde. 

Jetzt müssen ein paar Schritte durchgeführt werden...

- **eine Statusvariable wird gesetzt** 

````
dbEdit = true;
````

Da dieses Fenster zusätzlich noch zum bearbeiten lokaler Rezepte als auch zum Anlegen neuer Rezepte genutzt werden kann, muss festgelegt werden können wozu es aktuell genutzt wird. 

- **Die TextBoxen für Namen und Zubereitung werden befüllt**

````C#
 DataRowView row = (DataRowView)dgrRecipeDate.SelectedItem;
 TextRange dBtr = new TextRange(rtbxmanual.Document.ContentStart, rtbxmanual.Document.ContentEnd);

dBtr.Text = row["manual"].ToString();
tbxname.Text = row["name"].ToString();
````



- **Die Zutaten werden in die Liste eingetragen**

Um die Zutaten in eine Liste einzutragen, werden aus den Datenbankeinträgen lokale Ingredient-Objekte erzeugt. Diese werden dann in einer Liste gesammelt und als ItemsSource übergeben. 

Nachdem die gewünschten Änderungen gemacht worden sind, werden die Inhalte in der Datenbank angepasst. Bei den Rezepten ist es so, dass die Rezepte bearbeitet werden. Bei den Zutaten ist es allerdings leichter die aktuellen Zutaten zu löschen, die zu einem Identifier gehören, und im Anschluss daran neue anzulegen. 

#### Rezepte löschen 

Um Rezepte zu löschen muss wieder ein Rezept aus dem Datagrid gewählt werden. 

````C#
recipeTableAdapter.DeleteRecipe(editedIdentifier);
ingredientTableAdapter.DeleteIngredients(editedIdentifier);

recipeTableAdapter.Update(recipeDataTable);
recipeTableAdapter.Fill(recipeDataTable);

ingredientTableAdapter.Update(ingredientDataTable);
ingredientTableAdapter.Fill(ingredientDataTable);

````

Mithilfe der Funktion um Einträge zu löschen, können sowohl das Rezept als auch dessen Zutaten gelöscht werden. Anschließend muss wieder der TableAdapter aktualisiert werden. 

Damit sich das Datagrid der Zutaten auch leert, muss der DataContext temporär auf null gesetzt werden. Auch die Zubereitung soll sich leeren. 

````C#
dgrIngredient.DataContext = null;
tbDBIngredient.Text = "";
````

