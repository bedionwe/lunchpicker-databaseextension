﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RecipeKlassen
{
    public class Ingredient
    {
        #region Felder
        private double amount;
        private string type;
        #endregion

        #region Properties
        public double Amount
        {
            get { return amount; }
            set
            {   if (value <= 0)
                { MessageBox.Show("Positive Menge bei Zutaten nötig!"); }
                else
                { amount = value; }
            }
        }


        public string Type
        {
            get { return type; }
            set
            {
                if (String.IsNullOrWhiteSpace(value))
                {
                    throw new Exception("type darf nicht leer sein");
                }
                else
                {
                    type = value;
                }
            }
        }
        public Helper.Unit Unit { get; set; }
        #endregion

        #region Konstruktoren 
        public Ingredient()
        {

        }
        public Ingredient(double a, string t, Helper.Unit u) //Konstruktor
        {
            Amount = a;
            Type = t;
            Unit = u;
        }
        #endregion

        #region Methoden 
        public override string ToString()
        {
            return $"{Amount} {Unit} {Type}";
        }
        public string SerializeIng() //Trennzeichen #
        {
            return $"{Amount}#{Type}#{Unit}";
        }

        public static Ingredient ParseIng(string data)
        {
            string[] tokens = data.Split('#');
            double a = Double.Parse(tokens[0]);
            string t = tokens[1];
            Enum.TryParse(tokens[2], out Helper.Unit u);

            
            return new Ingredient(a, t, u);
     
        }

        #endregion
    }
}
 