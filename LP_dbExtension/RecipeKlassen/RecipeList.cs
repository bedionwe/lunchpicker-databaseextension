﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Automation.Peers;

namespace RecipeKlassen
{
    public class RecipeList
    {
        #region Felder
        private List<Recipe> items = new List<Recipe>();
        #endregion

        #region Properties

        public List<Recipe> Items 
        {
            get { return items; }
            set { items = value; }
        }


        #endregion

        #region Konstruktoren
        public RecipeList()
        {

        }
        #endregion

        #region Methoden
        public void AddRecipe(Recipe item)
        {
            items.Add(item);
        }

      
        public List<Recipe> OrderAlphAZ(List<Recipe> list) //Einfache sortieren von A-Z
        {
            var res2 = list.OrderBy(r => r.Name); //sortieren und in var zwischenspeichern

            List<Recipe> l = new List<Recipe>();
            foreach (var recipeorderalph in res2) //jedes rezept in var in eine Rezeptliste speichern
            {
                l.Add(recipeorderalph);
            }
            return l; //diese Rezeptliste zurück geben  
        }
        public List<Recipe> OrderAlphZA(List<Recipe> list)
        {
            var res2 = list.OrderByDescending(r => r.Name); // mit Descending wird Reihenfolge getauscht es wird " faslch" herum sortiert beginnt mit Z

            List<Recipe> l = new List<Recipe>();
            foreach (var recipeorderalph in res2)
            {
                l.Add(recipeorderalph);
            }
            return l;
        }

        public List<Recipe> OrderDateNewFirst(List<Recipe> list)
        {
            var res2 = list.OrderByDescending(r => r.Date); //hier descending verwenden da sonst die alten Rezepte ganz oben stehen

            List<Recipe> l = new List<Recipe>();
            foreach (var recipeorderalph in res2)
            {
                l.Add(recipeorderalph);
            }
            return l;
        }
        public List<Recipe> OrderDateOldFirst(List<Recipe> list)
        {
            var res2 = list.OrderBy(r => r.Date); //ohne Descending stehen die alten Rezepte oben 

            List<Recipe> l = new List<Recipe>();
            foreach (var recipeorderalph in res2)
            {
                l.Add(recipeorderalph);
            }
            return l;
        }


        public void Save(string filename)
        {
            StreamWriter sw = new StreamWriter(filename);
            foreach (Recipe item in items)
            {
                sw.WriteLine(item.SerializeForSave());
            }
            sw.Dispose();
        }

        public void Open(string filename)
        {

            try
            {
                StreamReader sr = new StreamReader(filename);
                items.Clear();
                while (!sr.EndOfStream)
                {
                    string data = sr.ReadLine();
                    Recipe r1 = Recipe.ParseForSave(data);
                    items.Add(r1);
                }
                sr.Dispose();
            }
            catch
            {
                return;
            }
            
        }
        #endregion
    }
}
