﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeKlassen
{
   public  class Helper
    {
        public enum Unit
        {
            g,
            ml,
            Stk,
            kg,
            dl,
            L,
            TL,
            Pck,
            EL,
            MS,
            dag,
            gestrichL,
            Oz,
            Becher,
            gehäufterL,
            Prise,
        }
    }
}
