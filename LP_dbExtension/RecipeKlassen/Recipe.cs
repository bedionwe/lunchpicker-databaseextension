﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeKlassen
{
    public class Recipe
    {
        

        #region Klassenvariabeln
        private List<Ingredient> ingredients;
        private string manual, name;
        private DateTime date;
        private Guid index; //jedes Rezept soll einen Einzigartigen Index haben
        private bool[] boollist= new bool[9];
        #endregion

        #region Properties
        public string Manual //Rezeptanleitung Property
        {
            get { return manual; }
            set { manual = value; }
        }

        public List<Ingredient> IngredientList //Speichern von Zutaten in einer Liste, einfach sortieren, und hinzufügen
        {
            get { return ingredients; }
            set { ingredients = value; } //Falls man alle mittels einer Lsite hinzufügen will 
        }
        
        public void AddIngridient(Ingredient ingridient) //Einzelene Zutat hinzufügen 
        {
            ingredients.Add(ingridient); 
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string NameCaseInsensitiv
        {
            get { return name.ToLower(); }
        }

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public Guid Index //Kennziffer Property
        {
            get { return index; }
            set { index = value; }
        }

        #region Bools, dem das Rezept entsprechen kann 
        public bool[] BoolList //damit man alle bools auf einmal im Kontruktor übergeben kann 
        {
            get { return boollist; }
            set { boollist = value; }
        }

        public bool Vegetarian //1. platz im Bool
        {
            get { return boollist[0]; }
            set { boollist[0] = value; }
        }

        public bool Vegan
        {
            get { return boollist[1]; }
            set { boollist[1] = value; }
        }
        public bool Fish
        {
            get { return boollist[2]; }
            set { boollist[2] = value; }
        }
        public bool Meat
        {
            get { return boollist[3]; }
            set { boollist[3] = value; }
        }
        public bool Muffin
        {
            get { return boollist[4]; }
            set { boollist[4] = value; }
        }
        public bool Cake
        {
            get { return boollist[5]; }
            set { boollist[5] = value; }
        }
        public bool Sweet
        {
            get { return boollist[6]; }
            set { boollist[6] = value; }
        }

        public bool Soup
        {
            get { return boollist[7]; }
            set { boollist[7] = value; }
        }
        public bool Fav
        {
            get { return boollist[8]; }
            set { boollist[8] = value; }
        }
        #endregion

        #endregion

        #region Konstruktoren
        public Recipe (List<Ingredient> ingred, string manu, string n, bool[] bl, DateTime d, Guid index)//Konstruktor, ohne Bools, diese müssen mit Porperties gesetzt werden damit es übersichtlich bleibt
        {
            Manual = manu;
            IngredientList = ingred;
            Name = n;
            BoolList = bl;
            Date = d;
            this.index = index;
        }
        public Recipe(List<Ingredient> ingred, string manu, string n, Guid index)//Konstruktor, ohne Bools, diese müssen mit Porperties gesetzt werden damit es übersichtlich bleibt
        {
            Manual = manu;
            IngredientList = ingred;
            Name = n;
            Date = DateTime.Today;
            this.index = index;
        }

        public Recipe(List<Ingredient> ingred, string manu, string n, DateTime d, Guid index)//Konstruktor, ohne Bools, diese müssen mit Porperties gesetzt werden damit es übersichtlich bleibt
        {
            Manual = manu;
            IngredientList = ingred;
            Name = n;
            Date = d;
            this.index = index;
        }
        public Recipe()
        { }

        public Recipe(Guid id, DateTime d, string n)
        {
            Index = id;
            Date = d;
            Name = n;
        }

        
        #endregion

        #region Methoden

        public string SerializeForSave()
        {
            string ing = "", boolli = "", withoutenter = "";
            foreach (Ingredient item in ingredients)
            {
                ing = $"{ing}{item.SerializeIng()}*";
            }

            foreach (bool item in boollist)
            {
                boolli = $"{boolli}{item.ToString()}*";
            }

            withoutenter = manual.Replace("\r\n", "<cr>");


            return $"{Name}§{withoutenter}§{Date}§{index}§{boolli}§{ing}";

        }

        public override string ToString()
        {
            return Name;
        }

        public static Recipe ParseForSave(string data)
        {
            string[] tokens = data.Split('§');
            string name = tokens[0];
            string manualwithoutenter = tokens[1];
            DateTime date = DateTime.Parse(tokens[2]);
            Guid index = Guid.Parse(tokens[3]);
            string[] boolList = tokens[4].Split('*');
            string ing = tokens[5];

            string manual = manualwithoutenter.Replace("<cr>", "\r\n");

            #region string zu Boolliste konvertieren
            bool[] bl = new bool[9]; //bool array zur übergabe                     
            for (int i = 0; i < 9; i++) // jedes bool array elemt füllen 
            {
                bl[i] = Convert.ToBoolean(boolList[i]); // in boolemlemt i wird mit dem stringbool element gefüllt nachdem es konvertiert wurde                                     
            }
            #endregion

            #region string zu Ingrediantsliste konvertieren

            List<Ingredient> ingri = new List<Ingredient>();
            string[] helper = ing.Split('*');

            for (int i = 0; i < helper.Length -1; i++)
            {
                ingri.Add(Ingredient.ParseIng(helper[i]));
            }
           

            #endregion
            return new Recipe(ingri, manual, name, bl, date, index);

        }

        #endregion
    }
}
