﻿using RecipeKlassen;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Halbjahreprojekt_v1
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Database init
        DataSet1 dataSet = new DataSet1();

        DataSet1TableAdapters.recipeTableAdapter recipeTableAdapter = new DataSet1TableAdapters.recipeTableAdapter();
        DataSet1.recipeDataTable recipeDataTable { get; set; }

        DataSet1TableAdapters.ingredientsTableAdapter ingredientTableAdapter = new DataSet1TableAdapters.ingredientsTableAdapter();
        DataSet1.ingredientsDataTable ingredientDataTable { get; set; }


        #endregion

        List<Ingredient> ingrediants = new List<Ingredient>();
        RecipeList items = new RecipeList();
        RecipeList RecipeFilter = new RecipeList();

        bool recipeChanged, bfilter = false;
        bool dbEdit = false;

        Guid recipeInProgress; // Identifikationskennzahl um zu ermitteln an welchem Rezept gearbeitet wird
        string editedIdentifier;
        int editedRecipeID;


        public MainWindow()
        {
            InitializeComponent();
            recipeDataTable = dataSet.recipe;
            recipeTableAdapter.Fill(recipeDataTable);            
            dgrRecipeDate.DataContext = recipeDataTable;

            ingredientDataTable = dataSet.ingredients;
            ingredientTableAdapter.Fill(ingredientDataTable);
          
            
        }
        #region information
        //Hinweis es wird nur mit Z-Indexen 1 und 10 gearbeitet 1 wenns nach hinten soll und 10 damit es angezeigt wird
        // es darf jeweils nur 1 Stackpanel 10 haben die anderen müssen mit 1 nach hinten gesetzt werden   

        #endregion

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            

            cbUnit.ItemsSource = Enum.GetValues(typeof(RecipeKlassen.Helper.Unit)); //inhalt der Komboboxen für Einheiten
            cbUnit.SelectedIndex = 0;
           
            items.Open("RezeptListe.csv"); //gespeicherte Elemente an Liste übergeben

            lbout.ItemsSource = items.Items; //Elemente an Listbox übergeben 

            ImageBrush imgBrush = new ImageBrush();
            imgBrush.ImageSource = new BitmapImage(new Uri(@"food.jpg", UriKind.Relative));
            guirecipe.Background = imgBrush;
            guirecipe.Background.Opacity = 0.2;
        }

        private void bnewrecipe_Click(object sender, RoutedEventArgs e) //wechsel in neues Rezpet Window damit man dort neues Rezpet anlegen kann 
        {
            Panel.SetZIndex(guimain, 1);
            Panel.SetZIndex(guinewrecipe, 10);
            Panel.SetZIndex(guirecipe, 1);
            Panel.SetZIndex(guirecipebook, 1);
            Panel.SetZIndex(guihelp, 1);
            Panel.SetZIndex(guiDB, 1);

            dbEdit = false;
            recipeChanged = false;
            lbxingridents.Items.Clear();
            ingrediants = new List<Ingredient>();

            TextRange tr = new TextRange(WatermarkManual.Document.ContentStart, WatermarkManual.Document.ContentEnd);
            TextRange trx = new TextRange(rtbxmanual.Document.ContentStart, rtbxmanual.Document.ContentEnd);

            trx.Text = "";
            tr.Text = "Zubereitung...";
            tbxname.Text = "";
            tbxamount.Text = "";
            tbxingredient.Text = "";
            

            WatermarkManual.Visibility = Visibility.Visible;
            WatermarkIngredient.Visibility = Visibility.Visible;
            WatermarkName.Visibility = Visibility.Visible;
            WatermarkAmount.Visibility = Visibility.Visible;
        }

        private void Home_Click(object sender, RoutedEventArgs e) //Zurück zum Startbildschirm
        {
            Panel.SetZIndex(guimain, 10);
            Panel.SetZIndex(guinewrecipe, 1);
            Panel.SetZIndex(guirecipe, 1);
            Panel.SetZIndex(guirecipebook, 1);
            Panel.SetZIndex(guihelp, 1);
            Panel.SetZIndex(guiDB, 1);

            cbvegetarian.IsChecked = false;
            cbvegan.IsChecked = false;
            cbfish.IsChecked = false;
            cbmeat.IsChecked = false;
            cbmuffin.IsChecked = false;
            cbcake.IsChecked = false;
            cbsweet.IsChecked = false;
            cbsoup.IsChecked = false;


        }

        private void Back_Click(object sender, RoutedEventArgs e) //Zurück ins Rezeptbuch
        {
            Panel.SetZIndex(guimain, 1);
            Panel.SetZIndex(guinewrecipe, 1);
            Panel.SetZIndex(guirecipe, 1);
            Panel.SetZIndex(guirecipebook, 10);
            Panel.SetZIndex(guihelp, 1);
            Panel.SetZIndex(guiDB, 1);
        }

        private void borderalph_Click(object sender, RoutedEventArgs e) //alphabetische Ordunung der Rezepte
        {
            if (bfilter == true)
            {
                RecipeFilter.Items = RecipeFilter.OrderAlphAZ(RecipeFilter.Items);
                lbout.ItemsSource = null;
                lbout.ItemsSource = RecipeFilter.Items;
            }
            else
            {
                items.Items = items.OrderAlphAZ(items.Items);
                lbout.ItemsSource = null;
                lbout.ItemsSource = items.Items;
            }

        }
        private void borderalphZ_A_Click(object sender, RoutedEventArgs e)
        {
            if (bfilter == true)
            {
                RecipeFilter.Items = RecipeFilter.OrderAlphZA(RecipeFilter.Items);
                lbout.ItemsSource = null;
                lbout.ItemsSource = RecipeFilter.Items;
            }
            else
            {
                items.Items = items.OrderAlphZA(items.Items);
                lbout.ItemsSource = null;
                lbout.ItemsSource = items.Items;
            }


        }

        private void borderdate_Click(object sender, RoutedEventArgs e) // die neuesten Rezepte zuerst
        {
            if (bfilter == true)
            {
                RecipeFilter.Items = RecipeFilter.OrderDateNewFirst(RecipeFilter.Items);
                lbout.ItemsSource = null;
                lbout.ItemsSource = RecipeFilter.Items;
            }
            else
            {
                items.Items = items.OrderDateNewFirst(items.Items);
                lbout.ItemsSource = null;
                lbout.ItemsSource = items.Items;
            }

        }
        private void borderdateold_Click(object sender, RoutedEventArgs e) // die neuesten Rezepte zuerst
        {
            if (bfilter == true)
            {
                RecipeFilter.Items = RecipeFilter.OrderDateNewFirst(RecipeFilter.Items);
                lbout.ItemsSource = null;
                lbout.ItemsSource = RecipeFilter.Items;
            }
            else
            {
                items.Items = items.OrderDateNewFirst(items.Items);
                lbout.ItemsSource = null;
                lbout.ItemsSource = items.Items;
            }


        }


        private void bchange_Click(object sender, RoutedEventArgs e)
        {
            //zur Bearbeitung eines Rezepts
            //Öffnet das Gui in dem Rezepte erstellt werden 
            //und auch bearbeitet werden können 
            Panel.SetZIndex(guimain, 1);
            Panel.SetZIndex(guinewrecipe, 10);
            Panel.SetZIndex(guirecipe, 1);
            Panel.SetZIndex(guirecipebook, 1);
            Panel.SetZIndex(guihelp, 1);
            Panel.SetZIndex(guiDB, 1);

            lbxingridents.Items.Clear();
            ingrediants = new List<Ingredient>();
            recipeChanged = true;
            dbEdit = false;

            WatermarkManual.Visibility = Visibility.Hidden;
            WatermarkIngredient.Visibility = Visibility.Hidden;
            WatermarkName.Visibility = Visibility.Hidden;
            WatermarkAmount.Visibility = Visibility.Hidden;

            foreach (Recipe item in items.Items)  
            {
                if (item.Index == recipeInProgress) //Rezeptliste abgleichen, mit welchem Rezept wird gearbeitet (über ID)
                {
                    foreach (Ingredient obj in item.IngredientList) // Zutaten an die LB übergeben
                    {
                        lbxingridents.Items.Add(obj);
                    }

                    ingrediants = item.IngredientList; //Textboxen beschreiben
                    TextRange tr = new TextRange(rtbxmanual.Document.ContentStart, rtbxmanual.Document.ContentEnd);

                    tr.Text = item.Manual;
                    tbxname.Text = item.Name;

                    cbfav.IsChecked = item.Fav;
                    cbvegetarian.IsChecked = item.Vegetarian;
                    cbvegan.IsChecked = item.Vegan;
                    cbmeat.IsChecked = item.Meat;
                    cbfish.IsChecked = item.Fish;
                    cbsoup.IsChecked = item.Soup;
                    cbsweet.IsChecked = item.Sweet;
                    cbcake.IsChecked = item.Cake;
                    cbmuffin.IsChecked = item.Muffin;

                    
                }
            }

       
        }

        private void baddigrident_Click(object sender, RoutedEventArgs e) //zutat hinzufügen zu Listbox
        {
            
            try
            {
                Ingredient ing = new Ingredient(double.Parse(tbxamount.Text), 
                    tbxingredient.Text, (Helper.Unit)cbUnit.SelectedIndex);
                ingrediants.Add(ing);

                
                lbxingridents.Items.Add(ing.ToString());
                tbxamount.Text = "";
                tbxingredient.Text = "";
            }
            catch
            {
                SystemSounds.Beep.Play();
                MessageBox.Show("Bitte für die Menge eine Zahl eingeben");

                tbxamount.Focus();
                tbxamount.SelectAll();
                return;
            }
            
        }

        private void bdeletigrident_Click(object sender, RoutedEventArgs e) //löscht ausgewählte zutat
        {
            if (lbxingridents.SelectedIndex < 0)
            {
                SystemSounds.Beep.Play();
                MessageBox.Show("Bitte eine Zutat wählen die gelöscht werden soll");
                return;
            }
            int i = lbxingridents.SelectedIndex;
            ingrediants.RemoveAt(i);
            lbxingridents.Items.RemoveAt(i);
            
            
        }

        private void bfinish_Click(object sender, RoutedEventArgs e) 
        { 
            if (dbEdit == true)
            {
                bfinish.IsEnabled = false;
                string dBmanual;
                TextRange dBtr = new TextRange(rtbxmanual.Document.ContentStart, rtbxmanual.Document.ContentEnd);
                dBmanual = Convert.ToString(dBtr.Text);

                if (string.IsNullOrEmpty(tbxname.Text))
                {
                    SystemSounds.Beep.Play();
                    MessageBox.Show("Bitte einen Rezeptnamen eintragen");

                    tbxname.Focus();
                    tbxname.SelectAll();
                    return;
                }

                string name = tbxname.Text;
                string date = DateTime.Now.ToShortDateString();

                recipeTableAdapter.UpdateRecipe(name, dBmanual, date, editedIdentifier, editedRecipeID);

                string type = tbxingredient.Text;
                
                string unit = ((Helper.Unit)cbUnit.SelectedIndex).ToString();
                
                for (int i = 0; i < dgrIngredient.Items.Count - 1; i++)           //-1 weil immer eine Zeile zu viel in der Tabelle ist 
                {
                    DataRowView rowTemp = (DataRowView)dgrIngredient.Items[i];
                    //Löschen der aktuellen Zutaten 
                    ingredientTableAdapter.DeleteIngredients(editedIdentifier);

                    //Zutaten neu erstellen 
                    foreach (Ingredient item in lbxingridents.Items)
                    {
                        ingredientTableAdapter.InsertIngredient(editedIdentifier, item.Type, Decimal.Parse(item.Amount.ToString()), unit);
                    }

                }


                dbEdit = false;

                recipeTableAdapter.Update(recipeDataTable);
                recipeTableAdapter.Fill(recipeDataTable);

                ingredientTableAdapter.Update(ingredientDataTable);
                ingredientTableAdapter.Fill(ingredientDataTable);

                Panel.SetZIndex(guimain, 1);
                Panel.SetZIndex(guinewrecipe, 1);
                Panel.SetZIndex(guirecipe, 1);
                Panel.SetZIndex(guirecipebook, 1);
                Panel.SetZIndex(guihelp, 1);
                Panel.SetZIndex(guiDB, 10);

                MessageBox.Show("Das Gericht wurde erfolgreich bearbeitet");
                bsaveDb.IsEnabled = true;


                return; 
                
            }
            
            
            
            //Rezept soll abgespeichert werden
            //Wenn ein Rezpet fertig ist soll es im der Rezept Gui angezeigt werden 
            Guid index;
            if (recipeChanged == true) // gibt es das Rezept schon, oder ist es ein neues?
            {
                index = recipeInProgress; // ID soll die selbe bleiben
            }
            else
            {
                index = Guid.NewGuid(); // Es wird eine neue ID generiert
            }

            string manual;
            TextRange tr = new TextRange(rtbxmanual.Document.ContentStart, rtbxmanual.Document.ContentEnd);
            manual =Convert.ToString(tr.Text);

            if (string.IsNullOrEmpty(tbxname.Text))
            {
                SystemSounds.Beep.Play();
                MessageBox.Show("Bitte einen Rezeptnamen eintragen");

                tbxname.Focus();
                tbxname.SelectAll();
                return;
            }


            Recipe r1 = new Recipe(ingrediants, manual, tbxname.Text, DateTime.Now, index);

            #region Boolabfrage r1
            r1.Fav = Convert.ToBoolean(cbfav.IsChecked);
            r1.Vegetarian = Convert.ToBoolean(cbvegetarian.IsChecked);
            r1.Vegan = Convert.ToBoolean(cbvegan.IsChecked);
            r1.Fish = Convert.ToBoolean(cbfish.IsChecked);
            r1.Meat = Convert.ToBoolean(cbmeat.IsChecked);
            r1.Muffin = Convert.ToBoolean(cbmuffin.IsChecked);
            r1.Cake = Convert.ToBoolean(cbcake.IsChecked);
            r1.Sweet = Convert.ToBoolean(cbsweet.IsChecked);
            r1.Soup = Convert.ToBoolean(cbsoup.IsChecked);
            #endregion

           

            #region Speichervorgang 

            MessageBoxResult result = MessageBox.Show("Hast du auch nichts vergessen?", "Speichern", MessageBoxButton.OKCancel);
            switch (result)
            {
                case MessageBoxResult.OK:

                    if (recipeChanged == false) //neues Rezept ?
                    {
                        items.AddRecipe(r1);
                        recipeInProgress = r1.Index;

                    }
                    else // Rezept bearbeitet
                    {
                        for (int i = 0; i < items.Items.Count; i++)
                        {
                            if (items.Items[i].Index == recipeInProgress)
                            {
                                items.Items[i] = r1; //aktuelles Rezept wird überschrieben
                                recipeInProgress = items.Items[i].Index;
                            }
                        }
                    }

                    items.Save("RezeptListe.csv");
                    
                    lbout.ItemsSource = null;  //ListBox aktualisieren
                    lbout.ItemsSource = items.Items;
                    #endregion
                    //Ausgabe des neuen Rezepts
                    foreach (Recipe item in items.Items) 
                    {
                        if (item.Index == recipeInProgress)
                        {
                            tboutname.Text = item.Name;
                            tboutingredient.Text = "";
                            tboutmanual.Text = item.Manual;
                            foreach (Ingredient obj in item.IngredientList)
                            {
                                tboutingredient.Text = tboutingredient.Text + "\u2022 " + obj.ToString() + "\n";
                            }


                            #region checkboxen eines fertigen Rezepts
                            cboutpanel.Children.Clear(); //Chechboxen sollen zurückgesetzt werden

                            if (item.Fav == true)
                            {

                                CheckBox cbfavcheck = new CheckBox();
                                cbfavcheck.Content = "Lieblingsgericht";

                                cbfavcheck.Margin = new Thickness(5);
                                cbfavcheck.Background = new SolidColorBrush(Colors.White);
                                cbfavcheck.Padding = new Thickness(2);
                                cbfavcheck.FontSize = 20;
                                cbfavcheck.Foreground = new SolidColorBrush(Colors.DarkRed);
                                cbfavcheck.IsEnabled = false;
                                cbfavcheck.IsChecked = true;
                                cboutpanel.Children.Add(cbfavcheck);
                            }

                            if (item.Vegetarian == true)
                            {

                                CheckBox cbvegicheck = new CheckBox();
                                cbvegicheck.Content = "Vegetarisch";

                                cbvegicheck.Margin = new Thickness(5);
                                cbvegicheck.Background = new SolidColorBrush(Colors.White);
                                cbvegicheck.Padding = new Thickness(2);
                                cbvegicheck.FontSize = 20;
                                cbvegicheck.Foreground = new SolidColorBrush(Colors.Black);
                                cbvegicheck.IsEnabled = false;
                                cbvegicheck.IsChecked = true;
                                cboutpanel.Children.Add(cbvegicheck);
                            }

                            if (item.Vegan == true)
                            {

                                CheckBox cbvegancheck = new CheckBox();
                                cbvegancheck.Content = "Vegan";

                                cbvegancheck.Margin = new Thickness(5);
                                cbvegancheck.Background = new SolidColorBrush(Colors.White);
                                cbvegancheck.Padding = new Thickness(2);
                                cbvegancheck.FontSize = 20;
                                cbvegancheck.Foreground = new SolidColorBrush(Colors.Black);
                                cbvegancheck.IsEnabled = false;
                                cbvegancheck.IsChecked = true;
                                cboutpanel.Children.Add(cbvegancheck);
                            }

                            if (item.Fish == true)
                            {

                                CheckBox cbvfishcheck = new CheckBox();
                                cbvfishcheck.Content = "Fisch";

                                cbvfishcheck.Margin = new Thickness(5);
                                cbvfishcheck.Background = new SolidColorBrush(Colors.White);
                                cbvfishcheck.Padding = new Thickness(2);
                                cbvfishcheck.FontSize = 20;
                                cbvfishcheck.Foreground = new SolidColorBrush(Colors.Black);
                                cbvfishcheck.IsEnabled = false;
                                cbvfishcheck.IsChecked = true;
                                cboutpanel.Children.Add(cbvfishcheck);
                            }

                            if (item.Meat == true)
                            {

                                CheckBox cbmeatcheck = new CheckBox();
                                cbmeatcheck.Content = "Fleisch";

                                cbmeatcheck.Margin = new Thickness(5);
                                cbmeatcheck.Background = new SolidColorBrush(Colors.White);
                                cbmeatcheck.Padding = new Thickness(2);
                                cbmeatcheck.FontSize = 20;
                                cbmeatcheck.Foreground = new SolidColorBrush(Colors.Black);
                                cbmeatcheck.IsEnabled = false;
                                cbmeatcheck.IsChecked = true;
                                cboutpanel.Children.Add(cbmeatcheck);
                            }

                            if (item.Muffin == true)
                            {

                                CheckBox cbmuffincheck = new CheckBox();
                                cbmuffincheck.Content = "Muffins";

                                cbmuffincheck.Margin = new Thickness(5);
                                cbmuffincheck.Background = new SolidColorBrush(Colors.White);
                                cbmuffincheck.Padding = new Thickness(2);
                                cbmuffincheck.FontSize = 20;
                                cbmuffincheck.Foreground = new SolidColorBrush(Colors.Black);
                                cbmuffincheck.IsEnabled = false;
                                cbmuffincheck.IsChecked = true;
                                cboutpanel.Children.Add(cbmuffincheck);
                            }

                            if (item.Cake == true)
                            {

                                CheckBox cbcakecheck = new CheckBox();
                                cbcakecheck.Content = "Kuchen";

                                cbcakecheck.Margin = new Thickness(5);
                                cbcakecheck.Background = new SolidColorBrush(Colors.White);
                                cbcakecheck.Padding = new Thickness(2);
                                cbcakecheck.FontSize = 20;
                                cbcakecheck.Foreground = new SolidColorBrush(Colors.Black);
                                cbcakecheck.IsEnabled = false;
                                cbcakecheck.IsChecked = true;
                                cboutpanel.Children.Add(cbcakecheck);
                            }

                            if (item.Sweet == true)
                            {

                                CheckBox cbsweetcheck = new CheckBox();
                                cbsweetcheck.Content = "Süßspeise";

                                cbsweetcheck.Margin = new Thickness(5);
                                cbsweetcheck.Background = new SolidColorBrush(Colors.White);
                                cbsweetcheck.Padding = new Thickness(2);
                                cbsweetcheck.FontSize = 20;
                                cbsweetcheck.Foreground = new SolidColorBrush(Colors.Black);
                                cbsweetcheck.IsEnabled = false;
                                cbsweetcheck.IsChecked = true;
                                cboutpanel.Children.Add(cbsweetcheck);
                            }

                            if (item.Soup == true)
                            {

                                CheckBox cbsoupcheck = new CheckBox();
                                cbsoupcheck.Content = "Suppe";

                                cbsoupcheck.Margin = new Thickness(5);
                                cbsoupcheck.Background = new SolidColorBrush(Colors.White);
                                cbsoupcheck.Padding = new Thickness(2);
                                cbsoupcheck.FontSize = 20;
                                cbsoupcheck.Foreground = new SolidColorBrush(Colors.Black);
                                cbsoupcheck.IsEnabled = false;
                                cbsoupcheck.IsChecked = true;
                                cboutpanel.Children.Add(cbsoupcheck);
                            }
                            #endregion
                            

                        }
                    }

                    break;
                case MessageBoxResult.Cancel:
                    return;

            }
            
            Panel.SetZIndex(guimain, 1);
            Panel.SetZIndex(guinewrecipe, 1);
            Panel.SetZIndex(guirecipe, 10);
            Panel.SetZIndex(guirecipebook, 1);
            Panel.SetZIndex(guihelp, 10);
            Panel.SetZIndex(guiDB, 1);
            bsaveDb.IsEnabled = true;
        }

        private void bsearchbook_Click(object sender, RoutedEventArgs e) // zu kochbuch um es durch zu sehen 
        {
            Panel.SetZIndex(guimain, 1);
            Panel.SetZIndex(guinewrecipe, 1);
            Panel.SetZIndex(guirecipe, 1);
            Panel.SetZIndex(guirecipebook, 10);
            Panel.SetZIndex(guihelp, 1);
            Panel.SetZIndex(guiDB, 1);
            bfilter = false; //falls vorher noch die Filterliste aktuell war jetzt nicht mehr 

            lbout.ItemsSource = null;
            lbout.ItemsSource = items.Items;
            recipeChanged = true;


        }

        private void bsearch_Click(object sender, RoutedEventArgs e)
        {//hiermiet wird das nach Kategorien gefilterte Kochbuch angezeigt
            Panel.SetZIndex(guimain, 1);
            Panel.SetZIndex(guinewrecipe, 1);
            Panel.SetZIndex(guirecipe, 1);
            Panel.SetZIndex(guirecipebook, 10);
            Panel.SetZIndex(guihelp, 1);
            Panel.SetZIndex(guiDB, 1);

            RecipeFilter.Items = items.Items;
            bfilter = true;            

            #region Filterabfrage
            if (cbcakefilter.IsChecked == true)
            {
                List<Recipe> Hilfsliste = new List<Recipe>();
                foreach (Recipe item in RecipeFilter.Items) //wenn nach Kuchen gefiltert wird
                {
                    if (item.Cake == true) // jedes item das Kuchen "ist"
                    { Hilfsliste.Add(item);}//in Sortierte Liste speichern 
                }
                RecipeFilter.Items = Hilfsliste;

            }
            if (cbfishfilter.IsChecked == true)
            {
                List<Recipe> Hilfsliste = new List<Recipe>();
                foreach (Recipe item in RecipeFilter.Items)
                {
                    if (item.Fish == true)
                    { Hilfsliste.Add(item); }
                }
                RecipeFilter.Items = Hilfsliste;
            }
            if (cbmeatfilter.IsChecked == true)
            {
                List<Recipe> Hilfsliste = new List<Recipe>();
                foreach (Recipe item in RecipeFilter.Items)
                {
                    if (item.Meat == true)
                    { Hilfsliste.Add(item); }
                }
                RecipeFilter.Items = Hilfsliste;
            }
            if (cbmuffinfilter.IsChecked == true)
            {
                List<Recipe> Hilfsliste = new List<Recipe>();
                foreach (Recipe item in RecipeFilter.Items)
                {
                    if (item.Muffin == true)
                    { Hilfsliste.Add(item); }
                }
                RecipeFilter.Items = Hilfsliste;
            }
            if (cbsoupfilter.IsChecked == true)
            {
                List<Recipe> Hilfsliste = new List<Recipe>();
                foreach (Recipe item in RecipeFilter.Items)
                {
                    if (item.Soup == true)
                    { Hilfsliste.Add(item); }
                }
                RecipeFilter.Items = Hilfsliste;
            }
            if (cbveganfilter.IsChecked == true)
            {
                List<Recipe> Hilfsliste = new List<Recipe>();
                foreach (Recipe item in RecipeFilter.Items)
                {
                    if (item.Vegan == true)
                    { Hilfsliste.Add(item); }
                }
                RecipeFilter.Items = Hilfsliste;

            }
            if (cbsweetfilter.IsChecked == true)
            {
                List<Recipe> Hilfsliste = new List<Recipe>();
                foreach (Recipe item in RecipeFilter.Items)
                {
                    if (item.Sweet == true)
                    { Hilfsliste.Add(item); }
                }
                RecipeFilter.Items = Hilfsliste;

            }
            if (cbvegetarischfilter.IsChecked == true)
            {
                List<Recipe> Hilfsliste = new List<Recipe>();
                foreach (Recipe item in RecipeFilter.Items)
                {
                    if (item.Vegetarian == true)
                    { Hilfsliste.Add(item); }
                }
                RecipeFilter.Items = Hilfsliste;
            }
            if (cbfavfilter.IsChecked == true)
            {
                List<Recipe> Hilfsliste = new List<Recipe>();
                foreach (Recipe item in RecipeFilter.Items)
                {
                    if (item.Fav == true)
                    { Hilfsliste.Add(item); }
                }
                RecipeFilter.Items = Hilfsliste;
            }
            #endregion
            lbout.ItemsSource = null;
            lbout.ItemsSource = RecipeFilter.Items;


        }


        private void lbout_MouseDoubleClick(object sender, MouseButtonEventArgs e) //Anzeigen eines Rezepts, welches man aus deiner Liste geöffnet hat
        {
            if (lbout.SelectedIndex < 0)
                return;
            Panel.SetZIndex(guimain, 1);
            Panel.SetZIndex(guinewrecipe, 1);
            Panel.SetZIndex(guirecipe, 10);
            Panel.SetZIndex(guirecipebook, 1);
            Panel.SetZIndex(guihelp, 10);
            Panel.SetZIndex(guiDB, 1);

            bsaveDb.IsEnabled = true;

            #region Hintergrund

            //ImageBrush ib = new ImageBrush(new BitmapImage(new Uri("food.jpg")));
            //guirecipe.Background = ib;
            //guirecipe.Background.Opacity = 0.3;

            #endregion

            Recipe inProgress = lbout.SelectedItem as Recipe;
            recipeInProgress = inProgress.Index;  //die ID des angeklickten Rezepts wird ermittelt


            //Anzeige des jeweiligen Rezepts
            foreach (Recipe item in items.Items)
            {
                if (item.Index == recipeInProgress)
                {
                    tboutname.Text = item.Name;
                    tboutingredient.Text = "";
                    tboutmanual.Text = item.Manual;

                    foreach (Ingredient obj in item.IngredientList)
                    {
                        tboutingredient.Text = tboutingredient.Text + "\u2022 " + obj.ToString() + "\n";
                    }

                    #region checkboxen eines fertigen Rezepts
                    cboutpanel.Children.Clear(); //Chechboxen sollen zurückgesetzt werden

                    if (item.Fav == true)
                    {

                        CheckBox cbfavcheck = new CheckBox();
                        cbfavcheck.Content = "Lieblingsgericht";

                        cbfavcheck.Margin = new Thickness(5);
                        cbfavcheck.Background = new SolidColorBrush(Colors.White);
                        cbfavcheck.Padding = new Thickness(2);
                        cbfavcheck.FontSize = 20;
                        cbfavcheck.Foreground = new SolidColorBrush(Colors.DarkRed);
                        cbfavcheck.IsEnabled = false;
                        cbfavcheck.IsChecked = true;
                        cboutpanel.Children.Add(cbfavcheck);
                    }

                    if (item.Vegetarian == true)
                    {

                        CheckBox cbvegicheck = new CheckBox();
                        cbvegicheck.Content = "Vegetarisch";

                        cbvegicheck.Margin = new Thickness(5);
                        cbvegicheck.Background = new SolidColorBrush(Colors.White);
                        cbvegicheck.Padding = new Thickness(2);
                        cbvegicheck.FontSize = 20;
                        cbvegicheck.Foreground = new SolidColorBrush(Colors.Black);
                        cbvegicheck.IsEnabled = false;
                        cbvegicheck.IsChecked = true;
                        cboutpanel.Children.Add(cbvegicheck);
                    }

                    if (item.Vegan == true)
                    {

                        CheckBox cbvegancheck = new CheckBox();
                        cbvegancheck.Content = "Vegan";

                        cbvegancheck.Margin = new Thickness(5);
                        cbvegancheck.Background = new SolidColorBrush(Colors.White);
                        cbvegancheck.Padding = new Thickness(2);
                        cbvegancheck.FontSize = 20;
                        cbvegancheck.Foreground = new SolidColorBrush(Colors.Black);
                        cbvegancheck.IsEnabled = false;
                        cbvegancheck.IsChecked = true;
                        cboutpanel.Children.Add(cbvegancheck);
                    }

                    if (item.Fish == true)
                    {

                        CheckBox cbvfishcheck = new CheckBox();
                        cbvfishcheck.Content = "Fisch";

                        cbvfishcheck.Margin = new Thickness(5);
                        cbvfishcheck.Background = new SolidColorBrush(Colors.White);
                        cbvfishcheck.Padding = new Thickness(2);
                        cbvfishcheck.FontSize = 20;
                        cbvfishcheck.Foreground = new SolidColorBrush(Colors.Black);
                        cbvfishcheck.IsEnabled = false;
                        cbvfishcheck.IsChecked = true;
                        cboutpanel.Children.Add(cbvfishcheck);
                    }

                    if (item.Meat == true)
                    {

                        CheckBox cbmeatcheck = new CheckBox();
                        cbmeatcheck.Content = "Fleisch";

                        cbmeatcheck.Margin = new Thickness(5);
                        cbmeatcheck.Background = new SolidColorBrush(Colors.White);
                        cbmeatcheck.Padding = new Thickness(2);
                        cbmeatcheck.FontSize = 20;
                        cbmeatcheck.Foreground = new SolidColorBrush(Colors.Black);
                        cbmeatcheck.IsEnabled = false;
                        cbmeatcheck.IsChecked = true;
                        cboutpanel.Children.Add(cbmeatcheck);
                    }

                    if (item.Muffin == true)
                    {

                        CheckBox cbmuffincheck = new CheckBox();
                        cbmuffincheck.Content = "Muffins";

                        cbmuffincheck.Margin = new Thickness(5);
                        cbmuffincheck.Background = new SolidColorBrush(Colors.White);
                        cbmuffincheck.Padding = new Thickness(2);
                        cbmuffincheck.FontSize = 20;
                        cbmuffincheck.Foreground = new SolidColorBrush(Colors.Black);
                        cbmuffincheck.IsEnabled = false;
                        cbmuffincheck.IsChecked = true;
                        cboutpanel.Children.Add(cbmuffincheck);
                    }

                    if (item.Cake == true)
                    {

                        CheckBox cbcakecheck = new CheckBox();
                        cbcakecheck.Content = "Kuchen";

                        cbcakecheck.Margin = new Thickness(5);
                        cbcakecheck.Background = new SolidColorBrush(Colors.White);
                        cbcakecheck.Padding = new Thickness(2);
                        cbcakecheck.FontSize = 20;
                        cbcakecheck.Foreground = new SolidColorBrush(Colors.Black);
                        cbcakecheck.IsEnabled = false;
                        cbcakecheck.IsChecked = true;
                        cboutpanel.Children.Add(cbcakecheck);
                    }

                    if (item.Sweet == true)
                    {

                        CheckBox cbsweetcheck = new CheckBox();
                        cbsweetcheck.Content = "Süßspeise";

                        cbsweetcheck.Margin = new Thickness(5);
                        cbsweetcheck.Background = new SolidColorBrush(Colors.White);
                        cbsweetcheck.Padding = new Thickness(2);
                        cbsweetcheck.FontSize = 20;
                        cbsweetcheck.Foreground = new SolidColorBrush(Colors.Black);
                        cbsweetcheck.IsEnabled = false;
                        cbsweetcheck.IsChecked = true;
                        cboutpanel.Children.Add(cbsweetcheck);
                    }

                    if (item.Soup == true)
                    {

                        CheckBox cbsoupcheck = new CheckBox();
                        cbsoupcheck.Content = "Suppe";

                        cbsoupcheck.Margin = new Thickness(5);
                        cbsoupcheck.Background = new SolidColorBrush(Colors.White);
                        cbsoupcheck.Padding = new Thickness(2);
                        cbsoupcheck.FontSize = 20;
                        cbsoupcheck.Foreground = new SolidColorBrush(Colors.Black);
                        cbsoupcheck.IsEnabled = false;
                        cbsoupcheck.IsChecked = true;
                        cboutpanel.Children.Add(cbsoupcheck);
                    }
                    #endregion

                }
               

            }
        }

        private void bdeleterecipe_Click(object sender, RoutedEventArgs e) //Löschen von Rezepten
        {
            //entscheidet von wo aus der Index zum löschen eines Rezeptes genommen wird 
            //denn abhängig vom Index wird ein anderes Rezept in der gesamten Liste verändert

            Recipe r = lbout.SelectedItem as Recipe;
            SystemSounds.Beep.Play();
            MessageBoxResult result = MessageBox.Show("Bist du dir sicher, dass du dieses " +
                "Rezept unwiederruflich löschen willst?", "Rezept Löschen?", MessageBoxButton.YesNo);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    for (int i = 0; i < items.Items.Count; i++) //Rezeptliste nach aktuellem Rezept durchsuchen
                    {
                        if (items.Items[i].Index == recipeInProgress) //stimmt die ID überein?
                        {
                            items.Items.RemoveAt(i); //löschen des Rezepts

                        }
                    }
                    lbout.ItemsSource = null;
                    lbout.ItemsSource = items.Items; // Listbox aktualisieren
                    items.Save("RezeptListe.csv"); //abspeichern

                    break;
                case MessageBoxResult.No:
                    return;

            }

            Panel.SetZIndex(guimain, 1);
            Panel.SetZIndex(guinewrecipe, 1);
            Panel.SetZIndex(guirecipe, 1);
            Panel.SetZIndex(guirecipebook, 10);
            Panel.SetZIndex(guihelp, 1);
            Panel.SetZIndex(guiDB, 1);


        }

        private void tbxsearch_TextChanged(object sender, TextChangedEventArgs e) //Rezept anhand der Namen Filtern
        {
            string sHelp = tbxsearch.Text;
            string s = sHelp.ToLower();
            bool contains;
            if (bfilter == true)
            {
                List<Recipe> Hilfsliste = new List<Recipe>();
                foreach (Recipe item in RecipeFilter.Items)
                {
                    contains = false;
                    contains = item.NameCaseInsensitiv.Contains(s);
                    if (contains == true)
                    {
                        Hilfsliste.Add(item);
                    }
                }
                RecipeFilter.Items = Hilfsliste;
            }
            else
            {
                List<Recipe> Hilfsliste = new List<Recipe>();
                foreach (Recipe item in items.Items)
                {
                    contains = false;
                    contains = item.NameCaseInsensitiv.Contains(s);
                    if (contains == true)
                    {
                        Hilfsliste.Add(item);
                    }
                }
                RecipeFilter.Items = Hilfsliste;
            }
            lbout.ItemsSource = null;
            lbout.ItemsSource = RecipeFilter.Items;
        }

        private void lbxingridents_MouseDoubleClick(object sender, MouseButtonEventArgs e) // Wenn eine Zutat bearbeitet werden soll
        {
            if (lbxingridents.SelectedIndex>=0)
            {
                if (dbEdit == true)
                {
                    int i = lbxingridents.SelectedIndex;
                    Ingredient ing = new Ingredient();
                    ing = (Ingredient)lbxingridents.Items[i];

                    EditIngredient dlg = new EditIngredient(ing);
                    if (dlg.ShowDialog().Value)
                    {
                        lbxingridents.Items.RemoveAt(i);
                        lbxingridents.Items.Insert(i, ing);
                        
                    }
                }
                else
                {
                    int i = lbxingridents.SelectedIndex;
                    //Ingredient ing = new Ingredient();
                    Ingredient ing = new Ingredient();
                    ing = ingrediants[i];
                    EditIngredient dlg = new EditIngredient(ing);
                    if (dlg.ShowDialog().Value)
                    {
                        lbxingridents.Items.RemoveAt(i);
                        ingrediants.RemoveAt(i);

                        lbxingridents.Items.Insert(i, ing);
                        ingrediants.Insert(i, ing);
                    }
                }
                
            }
           
        }

        #region Default Text in den Textboxen


        //Textbox Name
        private void WatermarkName_GotFocus(object sender, RoutedEventArgs e)
        {
            WatermarkName.Visibility = Visibility.Collapsed;
            tbxname.Visibility = Visibility.Visible;

            tbxname.Focus();
        }


        private void tbxname_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(tbxname.Text))
            {
                WatermarkName.Visibility = Visibility.Visible;
                tbxname.Visibility = Visibility.Collapsed;
            }
        }


        //Textbox Zubereitung

        private void WatermarkManual_GotFocus(object sender, RoutedEventArgs e)
        {
            WatermarkManual.Visibility = Visibility.Collapsed;
            rtbxmanual.Visibility = Visibility.Visible;

            rtbxmanual.Focus();
        }

        private void rtbxmanual_LostFocus(object sender, RoutedEventArgs e)
        {
            
            if (string.IsNullOrEmpty(rtbxmanual.ToString()))
            {
                
                WatermarkManual.Visibility = Visibility.Visible;
                rtbxmanual.Visibility = Visibility.Collapsed;
            }
        }


        //Textbox Zutat

        private void WatermarkIngredient_GotFocus(object sender, RoutedEventArgs e)
        {
            WatermarkIngredient.Visibility = Visibility.Collapsed;
            tbxingredient.Visibility = Visibility.Visible;

            tbxingredient.Focus();
        }

        private void tbxingredient_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(tbxingredient.Text))
            {
                WatermarkIngredient.Visibility = Visibility.Visible;
                tbxingredient.Visibility = Visibility.Collapsed;
            }
        }



        //Textbox Menge

        private void WatermarkAmount_GotFocus(object sender, RoutedEventArgs e)
        {
            WatermarkAmount.Visibility = Visibility.Collapsed;
            tbxamount.Visibility = Visibility.Visible;

            tbxamount.Focus();
        }

        

        private void tbxamount_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(tbxamount.Text))
            {
                WatermarkAmount.Visibility = Visibility.Visible;
                tbxamount.Visibility = Visibility.Collapsed;
            }
        }

        //Textbox Suchen
        private void WatermarkSearch_GotFocus(object sender, RoutedEventArgs e)
        {
            WatermarkSearch.Visibility = Visibility.Collapsed;
            tbxsearch.Visibility = Visibility.Visible;

            tbxsearch.Focus();
        }

       

        private void tbxsearch_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(tbxsearch.Text))
            {
                WatermarkSearch.Visibility = Visibility.Visible;
                tbxsearch.Visibility = Visibility.Collapsed;
            }
        }

 

        #endregion

        #region Database extension
        private void bsaveDb_Click(object sender, RoutedEventArgs e)
        {
            string name, manual, date, identifier;
            //string ingredients = "";
            Recipe rToSave = new Recipe();
            foreach (Recipe item in items.Items) //Nach dem Rezept suchen, welches gespeichert werden soll
            {
                if (item.Index == recipeInProgress)
                {
                    rToSave = item;
                }
            }
            identifier = rToSave.Index.ToString();

            //Gibt es das Rezept schon in der Datenbank ?
            foreach (var item in dgrRecipeDate.Items)
            {
                if(((DataRowView)item)["identifier"].ToString()==identifier)
                {
                    MessageBox.Show("Dieses Rezept existiert in der Datenbank bereits");
                    return;
                }
            }
           
            name = rToSave.Name;
            manual = rToSave.Manual;
            date = rToSave.Date.Date.ToShortDateString();
            

            
            foreach (Ingredient item in rToSave.IngredientList)
            {
                ingredientTableAdapter.InsertIngredient(identifier, item.Type,(decimal)item.Amount, item.Unit.ToString());
                
            }


            bsaveDb.IsEnabled = false;
            recipeTableAdapter.insertRecipe(name, manual, date, identifier);
            //ingredientTableAdapter.InsertIngredient(identifier, ingredients);
            MessageBox.Show("Ihr Gericht wurde in der Datenbank abgespeichert");
            bsaveDb.IsEnabled = false;

            recipeTableAdapter.Update(recipeDataTable);
            recipeTableAdapter.Fill(recipeDataTable);

            ingredientTableAdapter.Update(ingredientDataTable);
            ingredientTableAdapter.Fill(ingredientDataTable);

            Panel.SetZIndex(guimain, 1);
            Panel.SetZIndex(guinewrecipe, 1);
            Panel.SetZIndex(guirecipe, 1);
            Panel.SetZIndex(guirecipebook, 1);
            Panel.SetZIndex(guihelp, 1);
            Panel.SetZIndex(guiDB, 10);

        }

        private void dgrRecipeDate_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgrRecipeDate.SelectedIndex >= 0)
            {
                DataRowView row = (DataRowView)dgrRecipeDate.SelectedItem;
                editedIdentifier = row["identifier"].ToString();
                
                tbDBIngredient.Text = row["manual"].ToString();
                var tempIngredient = ingredientTableAdapter.GetIngForRec(editedIdentifier);
                dgrIngredient.DataContext = tempIngredient;
            }

        }

        private void bDBhome_Click(object sender, RoutedEventArgs e)
        {
            Panel.SetZIndex(guimain, 10);
            Panel.SetZIndex(guinewrecipe, 1);
            Panel.SetZIndex(guirecipe, 1);
            Panel.SetZIndex(guirecipebook, 1);
            Panel.SetZIndex(guihelp, 1);
            Panel.SetZIndex(guiDB, 1);
        }

        private void dDBEdit_Click(object sender, RoutedEventArgs e)
        {
            int selectedId = dgrRecipeDate.SelectedIndex;
            lbxingridents.Items.Clear();

            if (selectedId < 0)
            {
                MessageBox.Show("Bitte Wählen sie ein Rezept aus, das bearbeitet werden soll");
            }
            else
            {
                dbEdit = true;

                DataRowView row = (DataRowView)dgrRecipeDate.SelectedItem;
                editedIdentifier = row["identifier"].ToString();
                editedRecipeID = int.Parse(row["ID"].ToString());

                TextRange dBtr = new TextRange(rtbxmanual.Document.ContentStart, rtbxmanual.Document.ContentEnd);

                dBtr.Text = row["manual"].ToString();
                tbxname.Text = row["name"].ToString();
                
                
                List<Ingredient> ingTemList = new List<Ingredient>();
                //Zutaten in die ListBox einschreiben

                for (int i = 0; i < dgrIngredient.Items.Count-1; i++)           //-1 weil immer eine Zeile zu viel in der Tabelle ist 
                {
                    try
                    {
                        DataRowView rowTemp = (DataRowView)dgrIngredient.Items[i];
                        Ingredient temp = new Ingredient();
                        temp.Amount = double.Parse(rowTemp["amount"].ToString());
                        temp.Type = rowTemp["type"].ToString();
                        temp.Unit = (Helper.Unit)Enum.Parse(typeof(Helper.Unit), rowTemp["unit"].ToString());
                        ingTemList.Add(temp);
                        lbxingridents.Items.Add(temp);
                    }
                    catch
                    {
                        return;
                    }
                    
                }
               


                WatermarkManual.Visibility = Visibility.Hidden;
                WatermarkIngredient.Visibility = Visibility.Hidden;
                WatermarkName.Visibility = Visibility.Hidden;
                WatermarkAmount.Visibility = Visibility.Hidden;

                

                



                Panel.SetZIndex(guimain, 1);
                Panel.SetZIndex(guinewrecipe, 10);
                Panel.SetZIndex(guirecipe, 1);
                Panel.SetZIndex(guirecipebook, 1);
                Panel.SetZIndex(guihelp, 1);
                Panel.SetZIndex(guiDB, 1);



            }
                
        }

        private void bDbDelete_Click(object sender, RoutedEventArgs e)
        {
            if (dgrRecipeDate.SelectedIndex < 0)
            {
                MessageBox.Show("Wählen sie ein Rezept aus, das gelöscht werden soll");
                return;
            }

            System.Windows.Forms.DialogResult result = System.Windows.Forms.MessageBox.Show("Möchten Sie das Rezepte unwiderruflich löschen?", "Rezept löschen", System.Windows.Forms.MessageBoxButtons.YesNo);

            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                recipeTableAdapter.DeleteRecipe(editedIdentifier);
                ingredientTableAdapter.DeleteIngredients(editedIdentifier);


                recipeTableAdapter.Update(recipeDataTable);
                recipeTableAdapter.Fill(recipeDataTable);

                ingredientTableAdapter.Update(ingredientDataTable);
                ingredientTableAdapter.Fill(ingredientDataTable);

                dgrIngredient.DataContext = null;
                tbDBIngredient.Text = "";



               MessageBox.Show("Ihr Gericht wurde aus der Datenbank gelöscht");
            }
            else
            {
                return;
            }

            


        }

        private void dbView_Click(object sender, RoutedEventArgs e)
        {
            

            recipeTableAdapter.Update(recipeDataTable);
            recipeTableAdapter.Fill(recipeDataTable);

            ingredientTableAdapter.Update(ingredientDataTable);
            ingredientTableAdapter.Fill(ingredientDataTable);

            Panel.SetZIndex(guimain, 1);
            Panel.SetZIndex(guinewrecipe, 1);
            Panel.SetZIndex(guirecipe, 1);
            Panel.SetZIndex(guirecipebook, 1);
            Panel.SetZIndex(guihelp, 1);
            Panel.SetZIndex(guiDB, 10);
        }
        #endregion
    }


}
