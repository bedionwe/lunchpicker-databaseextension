﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RecipeKlassen;

namespace Halbjahreprojekt_v1
{
    /// <summary>
    /// Interaction logic for EditIngredient.xaml
    /// </summary>
    public partial class EditIngredient : Window
    {
       private Ingredient item;

        public Ingredient Item
        {
            get { return item; }
            set { item = value; }
        }

        public EditIngredient(Ingredient item)
        {
            InitializeComponent();
            this.item = item;
            tbxAmount.Text = item.Amount.ToString();
            tbxName.Text = item.Type;
            cbUnit.SelectedItem = item.Unit;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cbUnit.ItemsSource = Enum.GetValues(typeof(RecipeKlassen.Helper.Unit)); //inhalt der Komboboxen für Einheiten
            //cbUnit.SelectedIndex = 0;
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DialogResult = true;
                item.Amount = double.Parse(tbxAmount.Text);
                item.Type = tbxName.Text;
                item.Unit = (Helper.Unit)cbUnit.SelectedIndex;
            }
            catch
            {
                SystemSounds.Beep.Play();
                MessageBox.Show("Bitte für die Menge eine Zahl eingeben");

                tbxAmount.Focus();
                tbxAmount.SelectAll();
                return;
            }
            
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        
    }
}
