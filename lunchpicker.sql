-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 13, 2021 at 08:46 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lunchpicker`
--

-- --------------------------------------------------------

--
-- Table structure for table `ingredients`
--

CREATE TABLE `ingredients` (
  `ID` int(11) NOT NULL,
  `identifier` varchar(40) NOT NULL,
  `type` varchar(50) NOT NULL,
  `amount` double NOT NULL,
  `unit` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ingredients`
--

INSERT INTO `ingredients` (`ID`, `identifier`, `type`, `amount`, `unit`) VALUES
(1139, '5953c87e-5867-4501-8ea2-fe8621843874', 'Brötchen', 500, 'g'),
(1140, '5953c87e-5867-4501-8ea2-fe8621843874', 'Speck', 100, 'g'),
(1141, '5953c87e-5867-4501-8ea2-fe8621843874', 'Zwiebeln', 4, 'Stk'),
(1142, '5953c87e-5867-4501-8ea2-fe8621843874', 'Perseilie, gehackt', 5, 'EL'),
(1143, '5953c87e-5867-4501-8ea2-fe8621843874', 'Margerine', 40, 'g'),
(1144, '5953c87e-5867-4501-8ea2-fe8621843874', 'Eier', 6, 'Stk'),
(1145, '5953c87e-5867-4501-8ea2-fe8621843874', 'Butter', 50, 'g'),
(1146, '5953c87e-5867-4501-8ea2-fe8621843874', 'Fleischbrühe', 3, 'L'),
(1147, '5953c87e-5867-4501-8ea2-fe8621843874', 'Salz und Pfeffer', 1, 'Prise'),
(1172, '9c9ab424-076f-485b-b937-635ea850bca8', 'Butter', 200, 'g'),
(1173, '9c9ab424-076f-485b-b937-635ea850bca8', 'Zartbitterschokolade', 200, 'g'),
(1174, '9c9ab424-076f-485b-b937-635ea850bca8', 'Eier', 4, 'Stk'),
(1175, '9c9ab424-076f-485b-b937-635ea850bca8', 'gemahlene Mandeln', 200, 'g'),
(1176, '9c9ab424-076f-485b-b937-635ea850bca8', 'Zucker', 200, 'g'),
(1177, '9c9ab424-076f-485b-b937-635ea850bca8', 'Vanillezucker', 1, 'Pck'),
(1178, '9c9ab424-076f-485b-b937-635ea850bca8', 'Salz', 1, 'Prise'),
(1179, '9c9ab424-076f-485b-b937-635ea850bca8', 'Backpulver', 0.5, 'Pck'),
(1204, '9f574d79-a921-47ca-a3ca-3c87d025f523', 'Wasser', 3, 'L'),
(1205, '9f574d79-a921-47ca-a3ca-3c87d025f523', 'Salz', 1, 'Prise'),
(1206, '9f574d79-a921-47ca-a3ca-3c87d025f523', 'kleine Zucchini', 2, 'Stk'),
(1207, '9f574d79-a921-47ca-a3ca-3c87d025f523', 'Tomaten', 200, 'g'),
(1208, '9f574d79-a921-47ca-a3ca-3c87d025f523', 'Nudeln (Spaghetti)', 200, 'g'),
(1209, '9f574d79-a921-47ca-a3ca-3c87d025f523', 'Olivenöl', 2, 'EL'),
(1210, '9f574d79-a921-47ca-a3ca-3c87d025f523', 'Kräutersalz', 1, 'Prise'),
(1211, '9f574d79-a921-47ca-a3ca-3c87d025f523', 'Pfeffer', 1, 'Prise');

-- --------------------------------------------------------

--
-- Table structure for table `recipe`
--

CREATE TABLE `recipe` (
  `ID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `manual` varchar(800) NOT NULL,
  `date` varchar(16) NOT NULL,
  `identifier` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `recipe`
--

INSERT INTO `recipe` (`ID`, `name`, `manual`, `date`, `identifier`) VALUES
(20, 'Nudelpfanne mit buntem Gemüse', 'Bringe in einem Topf drei Liter Wasser zum Kochen.\r\nWasche das Gemüse während du darauf wartest, dass das Wasser zu kochen beginnt.\r\nSchneide die Zucchini und die Tomaten getrennt voneinander in kleine Stücke.\r\nSalze das Nudelwasser sobald es kocht und gib die Nudeln hinein.\r\nKoche die Nudeln laut Packungsanweisung al dente.\r\nErwärme währenddessen das Öl in der Pfanne und brate die Zucchini drei bis fünf Minuten hellbraun an. Rühre dabei immer wieder um, damit die Zucchini-Stücke nicht anbrennen.\r\nGib nun die Tomatenstücke dazu und brate sie für zwei bis drei Minuten mit an. Die Tomatenstücke sollen heiß sein, aber nicht zerfallen.\r\nGib die abgetropften Nudeln zum Gemüse in die Pfanne und vermenge die Zutaten miteinander.\r\nWürze die Nudelpfanne mit Kräutersalz und Pfeffer.\r\nDekoriere das s', '09.04.2021', '338b1aa8-c86a-499e-8f08-8e3bfae41e44'),
(21, 'Semmelknödel (8 Portionen)', 'Die Brötchen 2 - 3 Tage trocken lagern. Danach mit einem scharfen Brotmesser in ca. 1 x 1 cm große Würfel schneiden. In einer großen Schüssel offen unter öfterem Wenden 2 - 3 Tage trocknen lassen. Mancherorts bekommt man Semmelwürfel auch fertig zu kaufen.\r\n\r\nDie Zwiebeln schälen und fein würfeln. Die Petersilie waschen und fein schneiden. Die Margarine in einer Pfanne erhitzen und die Speckwürfel leicht darin anbraten. Die Zwiebelwürfel dazugeben und glasig anschwitzen. Die Petersilie dazugeben, alles verrühren, unter Deckel ca. 10 Min. weiterbraten und beiseitestellen.\r\n\r\nDie Eier verschlagen und mit Pfeffer und Salz würzen. In einem kleinen Topf Milch und Butter bis kurz vorm Kochen erhitzen. Die Semmelwürfel in einer großen Rührschüssel mit der heißen Milch übergießen und gut verrühren', '25.05.2020', '5953c87e-5867-4501-8ea2-fe8621843874'),
(25, 'Schokokuchen', 'Butter mit Schokolade schmelzen und mit allen anderen Zutaten mischen. Teig in eine gefettete Springform geben und bei 160 °C, 40 min backen.\r\nMit Puderzucker nach Belieben verzieren. \r\n\r\n\r\n', '25.06.2020', '9c9ab424-076f-485b-b937-635ea850bca8'),
(29, 'Nudelpfanne mit buntem Gemüse', 'Bringe in einem Topf drei Liter Wasser zum Kochen.\r\nWasche das Gemüse während du darauf wartest, dass das Wasser zu kochen beginnt.\r\nSchneide die Zucchini und die Tomaten getrennt voneinander in kleine Stücke.\r\nSalze das Nudelwasser sobald es kocht und gib die Nudeln hinein.\r\nKoche die Nudeln laut Packungsanweisung al dente.\r\nErwärme währenddessen das Öl in der Pfanne und brate die Zucchini drei bis fünf Minuten hellbraun an. Rühre dabei immer wieder um, damit die Zucchini-Stücke nicht anbrennen.\r\nGib nun die Tomatenstücke dazu und brate sie für zwei bis drei Minuten mit an. Die Tomatenstücke sollen heiß sein, aber nicht zerfallen.\r\nGib die abgetropften Nudeln zum Gemüse in die Pfanne und vermenge die Zutaten miteinander.\r\nWürze die Nudelpfanne mit Kräutersalz und Pfeffer.\r\n', '09.04.2021', '9f574d79-a921-47ca-a3ca-3c87d025f523');

-- --------------------------------------------------------

--
-- Stand-in structure for view `recipedate`
-- (See below for the actual view)
--
CREATE TABLE `recipedate` (
`date` varchar(16)
,`name` varchar(50)
,`ID` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `recipeingredients`
-- (See below for the actual view)
--
CREATE TABLE `recipeingredients` (
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `recipemanual`
-- (See below for the actual view)
--
CREATE TABLE `recipemanual` (
`identifier` varchar(40)
,`name` varchar(50)
,`manual` varchar(800)
);

-- --------------------------------------------------------

--
-- Structure for view `recipedate`
--
DROP TABLE IF EXISTS `recipedate`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `recipedate`  AS  select `recipe`.`date` AS `date`,`recipe`.`name` AS `name`,`recipe`.`ID` AS `ID` from `recipe` ;

-- --------------------------------------------------------

--
-- Structure for view `recipeingredients`
--
DROP TABLE IF EXISTS `recipeingredients`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `recipeingredients`  AS  select `recipe`.`name` AS `name`,`ingredients`.`actualingredient` AS `actualIngredient` from (`recipe` join `ingredients` on(`recipe`.`identifier` = `ingredients`.`identifier`)) ;

-- --------------------------------------------------------

--
-- Structure for view `recipemanual`
--
DROP TABLE IF EXISTS `recipemanual`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `recipemanual`  AS  select `recipe`.`identifier` AS `identifier`,`recipe`.`name` AS `name`,`recipe`.`manual` AS `manual` from `recipe` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ingredients`
--
ALTER TABLE `ingredients`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `recipe`
--
ALTER TABLE `recipe`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ingredients`
--
ALTER TABLE `ingredients`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1212;

--
-- AUTO_INCREMENT for table `recipe`
--
ALTER TABLE `recipe`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
